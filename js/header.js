header = {
    initialise: function() {
        var append = `
            <div class="header-logo-container">
                <div class="header-logo"></div>
            </div>
            <div class="header-nav">
                <div class="header-nav-container">
                    <div class="header-nav-icon-container">
                        <img src="images/journals.svg" class="header-nav-icon">
                    </div>
                    <div id="stage-gate-selection" class="header-nav-icon-container">
                        <img src="images/stage-gate.svg" class="header-nav-icon">
                    </div>
                </div>
            </div>`;

        $(`.header-container`).empty();
        $(`.header-container`).append(append);
        $(`#stage-gate-selection`).off();
        $(`#stage-gate-selection`).click(function () {
            stageGateSelect.setActive(true);
        });
    }
}