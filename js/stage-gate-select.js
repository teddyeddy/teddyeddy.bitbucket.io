stageGateSelect = {
    initialise: function() {
        var append = `
            <div class="stage-gate-parent-container">
                <div class="stage-gate-container">
                    <div class="stage-gate-action-bar">
                        <div id="stage-gate-select-close" class="header-nav-icon-container">
                            <img src="images/close.svg" class="header-nav-icon">
                        </div>
                    </div>
                    <div class="stage-gate-icon-row">
                        ${stageGateSelect.create.stageGate(`01-define`, `DEFINE`, `f9bd38`, true)}
                        ${stageGateSelect.create.stageGate(`02-concept`, `CONCEPT`, `ff7b22`, true)}
                        ${stageGateSelect.create.stageGate(`03-validate`, `VALIDATE`, `db311f`, false)}
                    </div>
                    <div class="stage-gate-icon-row">
                        ${stageGateSelect.create.stageGate(`04-build`, `BUILD`, `53962c`, false)}
                        ${stageGateSelect.create.stageGate(`05-release`, `RELEASE`, `3c78d8`, false)}
                        ${stageGateSelect.create.stageGate(`06-reflect`, `REFLECT`, `c45f99`, false)}
                    </div>
                </div>
            </div>
            <div class="stage-gate-background"></div>`; 

        $(`#stage-gate`).append(append);
        $(`#stage-gate-select-close`).off();
        $(`#stage-gate-select-close`).click(function () {
            stageGateSelect.setActive(false);
        });
    },
    setActive: function(active) {
        if (active == false) $(`#stage-gate`).hide();
        else if (active == true) $(`#stage-gate`).show();
    },
    create: {
        stageGate: function(icon, text, colour, active) {
            return `
                <div class="stage-gate-icon-container">
                    <div class="stage-gate-inactive" style="${active == true ? `display: none` : ``}"></div>
                    <div style="cursor: pointer">
                        <img src="images/stage-gates/${icon}.svg" class="stage-gate-icon">
                        <div class="stage-gate-text" style="color: #${colour}">${text}</div>
                    </div>
                </div>`;
        }
    }
}