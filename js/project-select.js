projectSelect = {
    initialise: function() {
        var append = `<div class="project-select-parent-container"></div>`;
        $(`.main`).append(append);
        projectSelect.create.projects();
    },
    create: {
        projects: function () {
            var colours = projectSelect.get.colours();
            var projects = projectSelect.get.projectsData();

            var append = ``;
            var colourIndex = 0;

            for (var i = 0; i < projects.length; i++){
                if (colourIndex > projects.length) colourIndex = 0;
                
                append += `
                    <div class="project-select-container" data-project="${projects[i].data}">
                        <div class ="project-ring-1" style="outline: 1px solid #${colours[colourIndex]};"></div>
                        <div class ="project-ring-2" style="outline: 1px solid #${colours[colourIndex]};"></div>
                        <div class="project" style="background: #${colours[colourIndex]}">
                            <img src="images/journals/${projects[i].icon}.svg" class="project-icon">
                        </div>
                        <div class="project-text">${projects[i].title}</div>
                    </div>`; 

                colourIndex += 1;
            }

            $(`.project-select-parent-container`).append(append);
            $(`.project-select-container`).click(function (e){
                $(`.project-select-parent-container`).hide();
                projectSelect.get.project($(e.currentTarget).data("project"));
            });
        }
    },
    get: {
        colours: function () {
            return [
                `7DE6FF`,   // Aqua
                `FFB900`,   // Honey
                `00DCB4`,   // Tiel
                `D59DFF`    // Lavender
            ];
        },
        project: function (project) {
            switch(project){
                case `tordenskjold`:
                    tordenskjold.initialise();
                    break;
            }
        },
        projectsData: function () {
            return [
                { title: `Heldig Hummer`, data: `heldighummer`, icon: `heldig-hummer` },
                { title: `Hexagems`, data: `hexagems`, icon: `hexagems` },
                { title: `Magic Brew`, data: `magicbrew`, icon: `magic-brew` },
                { title: `Tordenskjold`, data: `tordenskjold`, icon: `tordenskjold` }
            ];
        }
    }
}