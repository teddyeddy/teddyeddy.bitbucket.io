projects = {
    header: function(mainText, subText) {
        var ph = `project-header`

        return `
            <div class="${ph}-container">
                <div class="${ph}-text-container">
                    <div class="${ph}-main-text">${mainText}</div>
                    <div class="${ph}-sub-text">${subText}</div>
                </div>
                <div class="${ph}-background"></div>
            </div>`;
    },
    body: function() {
        return `
            <div class="project-body">
                <div class="project-resources-container">
                    <div class="project-resources-text">RESOURCES</div>
                </div>
            </div>`;
    }
}